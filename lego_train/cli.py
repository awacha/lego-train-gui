import sys
from PySide2 import QtWidgets
from .widgets.train import TrainControlWidget
from .widgets.motorcontrol import MotorControlWidget
from .widgets.motor_hacking import MotorHackingWidget
import click


def qtmainwithwidget(widgetclass):
    app = QtWidgets.QApplication(sys.argv)
    widget = widgetclass()
    widget.show()
    result = app.exec_()
    widget.deleteLater()
    app.deleteLater()
    sys.exit(result)

@click.group()
def mainprogram():
    pass

@mainprogram.command()
def train():
    return qtmainwithwidget(TrainControlWidget)

@mainprogram.command()
def motorhacking():
    return qtmainwithwidget(MotorHackingWidget)

@mainprogram.command()
def motorcontrol():
    return qtmainwithwidget(MotorControlWidget)

if __name__ == '__main__':
    mainprogram()