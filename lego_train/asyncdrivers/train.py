import multiprocessing
import queue
from typing import Any

import curio
import bricknil
import bricknil.sensor.motor
import bricknil.sensor.light
import bricknil.hub

class TrainCommand:
    command: str
    arguments: Any
    def __init__(self, command: str, *args):
        self.command = command
        self.arguments = args


@bricknil.attach(bricknil.sensor.motor.TrainMotor, name='motor')
@bricknil.attach(bricknil.sensor.light.LED, name='led')
#@bricknil.attach(bricknil.sensor.light.Light, name='light')
class Train(bricknil.hub.PoweredUpHub):
    motor: bricknil.sensor.motor.TrainMotor
    led: bricknil.sensor.light.LED
    light: bricknil.sensor.light.Light

    def __init__(self, name: str, cmdqueue: multiprocessing.Queue, replyqueue: multiprocessing.Queue):
        self.cmdqueue = cmdqueue
        self.replyqueue = replyqueue
        super().__init__(name)

    async def _get_from_queue_task(self):
        try:
            return self.cmdqueue.get(block=True)
        except curio.CancelledError:
            raise

    async def run(self):
        self.replyqueue.put(TrainCommand('online'))
        lastspeedreported = None
        while True:
            await curio.sleep(0.01)
            try:
                message = self.cmdqueue.get_nowait()
            except queue.Empty:
                pass
            else:
                if message.command == 'exit':
                    return
                elif message.command == 'setspeed':
                    await self.motor.set_speed(message.arguments[0])
                elif message.command == 'setled':
                    await self.led.set_color(message.arguments[0])
                elif message.command == 'ramp':
                    await self.motor.ramp_speed(target_speed=message.arguments[1], ramp_time_ms=int(message.arguments[0]*1000))
                elif message.command == 'setbrightness':
                    await self.light.set_brightness(message.arguments[0])
                    self.replyqueue.put(TrainCommand('brightnessreply', message.arguments[0]))
            if self.motor.speed != lastspeedreported:
                lastspeedreported = self.motor.speed
                self.replyqueue.put(TrainCommand('speedreply', self.motor.speed))



