import multiprocessing
import queue
from typing import Any

import curio
import bricknil
import bricknil.sensor.sensor
import bricknil.hub


class RemoteCommand:
    command: str
    arguments: Any
    def __init__(self, command: str, *args):
        self.command = command
        self.arguments = args


@bricknil.attach(bricknil.sensor.sensor.RemoteButtons, name='leftbuttons', port=bricknil.sensor.sensor.RemoteButtons.Port.L, capabilities=['sense_press'])
@bricknil.attach(bricknil.sensor.sensor.RemoteButtons, name='rightbuttons', port=bricknil.sensor.sensor.RemoteButtons.Port.R, capabilities=['sense_press'])
@bricknil.attach(bricknil.sensor.sensor.Button, name='greenbutton', capabilities=['sense_press'])
@bricknil.attach(bricknil.sensor.sensor.VoltageSensor, name='voltage', capabilities=['sense_s'])
@bricknil.attach(bricknil.sensor.sensor.CurrentSensor, name='current', capabilities=['sense_s'])
class RemoteControl(bricknil.hub.PoweredUpRemote):
    leftbuttons: bricknil.sensor.sensor.RemoteButtons
    rightbuttons: bricknil.sensor.sensor.RemoteButtons
    led: bricknil.sensor.light.LED
    greenbutton: bricknil.sensor.sensor.Button
    voltage: bricknil.sensor.VoltageSensor
    current: bricknil.sensor.CurrentSensor

    def __init__(self, name: str, cmdqueue: multiprocessing.Queue, replyqueue: multiprocessing.Queue):
        self.cmdqueue = cmdqueue
        self.replyqueue = replyqueue
        super().__init__(name)

    async def leftbuttons_change(self):
        self.replyqueue.put_nowait(RemoteCommand('leftbuttons', self.leftbuttons.value[bricknil.sensor.sensor.RemoteButtons.capability.sense_press]))

    async def rightbuttons_change(self):
        self.replyqueue.put_nowait(RemoteCommand('rightbuttons', self.rightbuttons.value[bricknil.sensor.sensor.RemoteButtons.capability.sense_press]))

    async def greenbutton_change(self):
        self.replyqueue.put_nowait(RemoteCommand('greenbutton', self.greenbutton.value[bricknil.sensor.sensor.Button.capability.sense_press]))

    async def voltage_change(self):
        self.replyqueue.put_nowait(RemoteCommand('voltage', self.voltage.value[bricknil.sensor.sensor.VoltageSensor.capability.sense_s]))

    async def current_change(self):
        self.replyqueue.put_nowait(RemoteCommand('current', self.current.value[bricknil.sensor.sensor.CurrentSensor.capability.sense_s]))

    async def _get_from_queue_task(self):
        try:
            return self.cmdqueue.get(block=True)
        except curio.CancelledError:
            raise

    def _get_motor(self, motorindex: int) -> bricknil.sensor.motor.CPlusLargeMotor:
        return [self.motor_a, self.motor_b][motorindex]

    async def run(self):
        self.replyqueue.put_nowait(RemoteCommand('online'))
        while True:
            await curio.sleep(0.01)
            try:
                message: RemoteCommand = self.cmdqueue.get_nowait()
            except queue.Empty:
                pass
            else:
                if message.command == 'exit':
                    # send ShutDown message
                    await self.send_message('shutdown', [0, 2, 2])

#                    await self.message_queue.put(b'\x04\x00\x02\x01')
                    return

def runsubprocess(name, cmdqueue, replyqueue):
    async def system():
        hub = RemoteControl(name, cmdqueue, replyqueue)
    bricknil.start(system)



if __name__ == '__main__':
    async def system():
        train = RemoteControl('Remote', multiprocessing.Queue(), multiprocessing.Queue())
    bricknil.start(system)
