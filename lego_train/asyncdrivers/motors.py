import multiprocessing
import queue
from typing import Any

import bricknil
import bricknil.hub
import bricknil.sensor.light
import bricknil.sensor.motor
import curio


class MotorHubCommand:
    command: str
    arguments: Any

    def __init__(self, command: str, *args):
        self.command = command
        self.arguments = args


@bricknil.attach(bricknil.sensor.motor.CPlusLargeMotor, name='motor0', port=0,
                 capabilities=['sense_pos', 'sense_speed'])
@bricknil.attach(bricknil.sensor.motor.CPlusLargeMotor, name='motor1', port=1,
                 capabilities=['sense_pos', 'sense_speed'])
@bricknil.attach(bricknil.sensor.light.LED, name='led')
@bricknil.attach(bricknil.sensor.sensor.Button, name='greenbutton', capabilities=['sense_press'])
@bricknil.attach(bricknil.sensor.sensor.VoltageSensor, name='voltage', capabilities=['sense_s'])
@bricknil.attach(bricknil.sensor.sensor.CurrentSensor, name='current', capabilities=['sense_s'])
class MotorsHub(bricknil.hub.PoweredUpHub):
    motor0: bricknil.sensor.motor.CPlusLargeMotor
    motor1: bricknil.sensor.motor.CPlusLargeMotor
    led: bricknil.sensor.light.LED
    greenbutton: bricknil.sensor.sensor.Button
    voltage: bricknil.sensor.VoltageSensor
    current: bricknil.sensor.CurrentSensor

    def __init__(self, name: str, cmdqueue: multiprocessing.Queue, replyqueue: multiprocessing.Queue):
        self.cmdqueue = cmdqueue
        self.replyqueue = replyqueue
        super().__init__(name)

    async def motor0_change(self):
        self.notify_pos_speed(0)

    def notify_pos_speed(self, motorindex):
        motor = self.motor0 if motorindex == 0 else self.motor1
        pos = motor.value[bricknil.sensor.motor.CPlusLargeMotor.capability.sense_pos]
        speed = motor.value[bricknil.sensor.motor.CPlusLargeMotor.capability.sense_speed]
        if pos >= 1 << 31:
            pos -= 1 << 32
        if speed >= 1 << 7:
            speed -= 1 << 8
        self.replyqueue.put_nowait(MotorHubCommand(f'motor{motorindex}', pos, speed))

    async def motor1_change(self):
        self.notify_pos_speed(1)

    async def greenbutton_change(self):
        self.replyqueue.put_nowait(MotorHubCommand('greenbutton', self.greenbutton.value[
            bricknil.sensor.sensor.Button.capability.sense_press]))

    async def voltage_change(self):
        self.replyqueue.put_nowait(
            MotorHubCommand('voltage', self.voltage.value[bricknil.sensor.sensor.VoltageSensor.capability.sense_s]))

    async def current_change(self):
        self.replyqueue.put_nowait(
            MotorHubCommand('current', self.current.value[bricknil.sensor.sensor.CurrentSensor.capability.sense_s]))

    async def _get_from_queue_task(self):
        try:
            return self.cmdqueue.get(block=True)
        except curio.CancelledError:
            raise

    def _get_motor(self, motorindex: int) -> bricknil.sensor.motor.CPlusLargeMotor:
        return [self.motor0, self.motor1][motorindex]

    async def run(self):
        self.replyqueue.put_nowait(MotorHubCommand('online'))
        lastspeedreported = None
        await self.motor0.set_speed(0)
        await self.motor1.set_speed(0)
        while True:
            await curio.sleep(0.01)
            try:
                message: MotorHubCommand = self.cmdqueue.get_nowait()
            except queue.Empty:
                pass
            else:
#                print(message.command, message.arguments)
                if message.command == 'exit':
                    # send ShutDown message
                    await self.send_message('shutdown',  [0, 2, 1])
                    break
                elif message.command == 'set_pos':
                    motorindex, position, speed, max_power = message.arguments
                    await self._get_motor(motorindex).set_pos(position, speed, max_power)
                elif message.command == 'set_speed':
                    motorindex, speed = message.arguments
                    await self._get_motor(motorindex).set_speed(speed)
 #                   print(f'Speed set. Motor: {motorindex}')
                elif message.command == 'ramp_speed':
                    motorindex, targetspeed, ramp_time_ms = message.arguments
                    await self._get_motor(motorindex).ramp_speed(targetspeed, ramp_time_ms)
                elif message.command == 'rotate':
                    motorindex, degrees, speed, max_power = message.arguments
                    await self._get_motor(motorindex).rotate(degrees, speed, max_power)
                elif message.command == 'stop':
                    motorindex = message.arguments[0]
                    await self._get_motor(motorindex).set_speed(0)
        return


def runsubprocess(name, cmdqueue, replyqueue):
    async def system():
        hub = MotorsHub(name, cmdqueue, replyqueue)

    bricknil.start(system)


if __name__ == '__main__':
    MotorsHub.runsubprocess('Motors', multiprocessing.Queue(), multiprocessing.Queue())
