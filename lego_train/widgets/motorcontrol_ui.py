# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'motorcontrol.ui'
##
## Created by: Qt User Interface Compiler version 5.15.6
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *  # type: ignore
from PySide2.QtGui import *  # type: ignore
from PySide2.QtWidgets import *  # type: ignore


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(482, 310)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.motorSettingsGroupBox = QGroupBox(Form)
        self.motorSettingsGroupBox.setObjectName(u"motorSettingsGroupBox")

        self.verticalLayout.addWidget(self.motorSettingsGroupBox)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.groupBox = QGroupBox(Form)
        self.groupBox.setObjectName(u"groupBox")
        self.horizontalLayout_2 = QHBoxLayout(self.groupBox)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.horizontalSpacer = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer)

        self.verticalLayout_2 = QVBoxLayout()
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.motorAUpToolButton = QToolButton(self.groupBox)
        self.motorAUpToolButton.setObjectName(u"motorAUpToolButton")
        icon = QIcon()
        iconThemeName = u"list-add"
        if QIcon.hasThemeIcon(iconThemeName):
            icon = QIcon.fromTheme(iconThemeName)
        else:
            icon.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.motorAUpToolButton.setIcon(icon)

        self.verticalLayout_2.addWidget(self.motorAUpToolButton)

        self.motorAStopToolButton = QToolButton(self.groupBox)
        self.motorAStopToolButton.setObjectName(u"motorAStopToolButton")
        icon1 = QIcon()
        iconThemeName = u"process-stop"
        if QIcon.hasThemeIcon(iconThemeName):
            icon1 = QIcon.fromTheme(iconThemeName)
        else:
            icon1.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.motorAStopToolButton.setIcon(icon1)

        self.verticalLayout_2.addWidget(self.motorAStopToolButton)

        self.motorADownToolButton = QToolButton(self.groupBox)
        self.motorADownToolButton.setObjectName(u"motorADownToolButton")
        icon2 = QIcon()
        iconThemeName = u"list-remove"
        if QIcon.hasThemeIcon(iconThemeName):
            icon2 = QIcon.fromTheme(iconThemeName)
        else:
            icon2.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.motorADownToolButton.setIcon(icon2)

        self.verticalLayout_2.addWidget(self.motorADownToolButton)


        self.horizontalLayout_2.addLayout(self.verticalLayout_2)

        self.horizontalSpacer_3 = QSpacerItem(40, 20, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_3)

        self.verticalLayout_3 = QVBoxLayout()
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.motorBUpToolButton = QToolButton(self.groupBox)
        self.motorBUpToolButton.setObjectName(u"motorBUpToolButton")
        self.motorBUpToolButton.setIcon(icon)

        self.verticalLayout_3.addWidget(self.motorBUpToolButton)

        self.motorBStopToolButton = QToolButton(self.groupBox)
        self.motorBStopToolButton.setObjectName(u"motorBStopToolButton")
        self.motorBStopToolButton.setIcon(icon1)

        self.verticalLayout_3.addWidget(self.motorBStopToolButton)

        self.motorBDownToolButton = QToolButton(self.groupBox)
        self.motorBDownToolButton.setObjectName(u"motorBDownToolButton")
        self.motorBDownToolButton.setIcon(icon2)

        self.verticalLayout_3.addWidget(self.motorBDownToolButton)


        self.horizontalLayout_2.addLayout(self.verticalLayout_3)

        self.horizontalSpacer_2 = QSpacerItem(0, 0, QSizePolicy.Expanding, QSizePolicy.Minimum)

        self.horizontalLayout_2.addItem(self.horizontalSpacer_2)


        self.horizontalLayout.addWidget(self.groupBox)

        self.groupBox_2 = QGroupBox(Form)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.horizontalLayout_3 = QHBoxLayout(self.groupBox_2)
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.logTreeView = QTreeView(self.groupBox_2)
        self.logTreeView.setObjectName(u"logTreeView")

        self.horizontalLayout_3.addWidget(self.logTreeView)

        self.verticalLayout_4 = QVBoxLayout()
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.logClearToolButton = QToolButton(self.groupBox_2)
        self.logClearToolButton.setObjectName(u"logClearToolButton")
        icon3 = QIcon()
        iconThemeName = u"edit-clear-all"
        if QIcon.hasThemeIcon(iconThemeName):
            icon3 = QIcon.fromTheme(iconThemeName)
        else:
            icon3.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.logClearToolButton.setIcon(icon3)

        self.verticalLayout_4.addWidget(self.logClearToolButton)

        self.logLoadToolButton = QToolButton(self.groupBox_2)
        self.logLoadToolButton.setObjectName(u"logLoadToolButton")
        icon4 = QIcon(QIcon.fromTheme(u"document-open"))
        self.logLoadToolButton.setIcon(icon4)

        self.verticalLayout_4.addWidget(self.logLoadToolButton)

        self.logSaveToolButton = QToolButton(self.groupBox_2)
        self.logSaveToolButton.setObjectName(u"logSaveToolButton")
        icon5 = QIcon()
        iconThemeName = u"document-save"
        if QIcon.hasThemeIcon(iconThemeName):
            icon5 = QIcon.fromTheme(iconThemeName)
        else:
            icon5.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.logSaveToolButton.setIcon(icon5)

        self.verticalLayout_4.addWidget(self.logSaveToolButton)

        self.logRecordToolButton = QToolButton(self.groupBox_2)
        self.logRecordToolButton.setObjectName(u"logRecordToolButton")
        icon6 = QIcon()
        iconThemeName = u"media-record"
        if QIcon.hasThemeIcon(iconThemeName):
            icon6 = QIcon.fromTheme(iconThemeName)
        else:
            icon6.addFile(u".", QSize(), QIcon.Normal, QIcon.Off)
        
        self.logRecordToolButton.setIcon(icon6)
        self.logRecordToolButton.setCheckable(True)

        self.verticalLayout_4.addWidget(self.logRecordToolButton)

        self.logReplayToolButton = QToolButton(self.groupBox_2)
        self.logReplayToolButton.setObjectName(u"logReplayToolButton")
        icon7 = QIcon(QIcon.fromTheme(u"media-playback-start"))
        self.logReplayToolButton.setIcon(icon7)
        self.logReplayToolButton.setCheckable(True)

        self.verticalLayout_4.addWidget(self.logReplayToolButton)


        self.horizontalLayout_3.addLayout(self.verticalLayout_4)


        self.horizontalLayout.addWidget(self.groupBox_2)


        self.verticalLayout.addLayout(self.horizontalLayout)

        QWidget.setTabOrder(self.motorAUpToolButton, self.motorAStopToolButton)
        QWidget.setTabOrder(self.motorAStopToolButton, self.motorADownToolButton)
        QWidget.setTabOrder(self.motorADownToolButton, self.motorBUpToolButton)
        QWidget.setTabOrder(self.motorBUpToolButton, self.motorBStopToolButton)
        QWidget.setTabOrder(self.motorBStopToolButton, self.motorBDownToolButton)
        QWidget.setTabOrder(self.motorBDownToolButton, self.logTreeView)
        QWidget.setTabOrder(self.logTreeView, self.logClearToolButton)
        QWidget.setTabOrder(self.logClearToolButton, self.logLoadToolButton)
        QWidget.setTabOrder(self.logLoadToolButton, self.logSaveToolButton)
        QWidget.setTabOrder(self.logSaveToolButton, self.logRecordToolButton)
        QWidget.setTabOrder(self.logRecordToolButton, self.logReplayToolButton)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.motorSettingsGroupBox.setTitle(QCoreApplication.translate("Form", u"Motorok be\u00e1ll\u00edt\u00e1sa:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"T\u00e1vir\u00e1ny\u00edt\u00f3", None))
        self.motorAUpToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.motorAStopToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.motorADownToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.motorBUpToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.motorBStopToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.motorBDownToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Napl\u00f3", None))
        self.logClearToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.logLoadToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.logSaveToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.logRecordToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.logReplayToolButton.setText(QCoreApplication.translate("Form", u"...", None))
    # retranslateUi

