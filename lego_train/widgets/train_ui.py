# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'train.ui'
##
## Created by: Qt User Interface Compiler version 5.15.6
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *  # type: ignore
from PySide2.QtGui import *  # type: ignore
from PySide2.QtWidgets import *  # type: ignore


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(724, 270)
        self.gridLayout_3 = QGridLayout(Form)
        self.gridLayout_3.setObjectName(u"gridLayout_3")
        self.ledGroupBox = QGroupBox(Form)
        self.ledGroupBox.setObjectName(u"ledGroupBox")
        self.horizontalLayout_2 = QHBoxLayout(self.ledGroupBox)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.toolButton = QToolButton(self.ledGroupBox)
        self.toolButton.setObjectName(u"toolButton")

        self.horizontalLayout_2.addWidget(self.toolButton)

        self.toolButton_2 = QToolButton(self.ledGroupBox)
        self.toolButton_2.setObjectName(u"toolButton_2")

        self.horizontalLayout_2.addWidget(self.toolButton_2)

        self.toolButton_3 = QToolButton(self.ledGroupBox)
        self.toolButton_3.setObjectName(u"toolButton_3")

        self.horizontalLayout_2.addWidget(self.toolButton_3)

        self.toolButton_4 = QToolButton(self.ledGroupBox)
        self.toolButton_4.setObjectName(u"toolButton_4")

        self.horizontalLayout_2.addWidget(self.toolButton_4)

        self.toolButton_5 = QToolButton(self.ledGroupBox)
        self.toolButton_5.setObjectName(u"toolButton_5")

        self.horizontalLayout_2.addWidget(self.toolButton_5)

        self.toolButton_6 = QToolButton(self.ledGroupBox)
        self.toolButton_6.setObjectName(u"toolButton_6")

        self.horizontalLayout_2.addWidget(self.toolButton_6)

        self.toolButton_7 = QToolButton(self.ledGroupBox)
        self.toolButton_7.setObjectName(u"toolButton_7")

        self.horizontalLayout_2.addWidget(self.toolButton_7)

        self.toolButton_8 = QToolButton(self.ledGroupBox)
        self.toolButton_8.setObjectName(u"toolButton_8")

        self.horizontalLayout_2.addWidget(self.toolButton_8)

        self.toolButton_9 = QToolButton(self.ledGroupBox)
        self.toolButton_9.setObjectName(u"toolButton_9")

        self.horizontalLayout_2.addWidget(self.toolButton_9)

        self.toolButton_10 = QToolButton(self.ledGroupBox)
        self.toolButton_10.setObjectName(u"toolButton_10")

        self.horizontalLayout_2.addWidget(self.toolButton_10)

        self.toolButton_11 = QToolButton(self.ledGroupBox)
        self.toolButton_11.setObjectName(u"toolButton_11")

        self.horizontalLayout_2.addWidget(self.toolButton_11)


        self.gridLayout_3.addWidget(self.ledGroupBox, 2, 0, 1, 1)

        self.groupBox_2 = QGroupBox(Form)
        self.groupBox_2.setObjectName(u"groupBox_2")
        self.gridLayout = QGridLayout(self.groupBox_2)
        self.gridLayout.setObjectName(u"gridLayout")
        self.rampTimeDoubleSpinBox = QDoubleSpinBox(self.groupBox_2)
        self.rampTimeDoubleSpinBox.setObjectName(u"rampTimeDoubleSpinBox")
        self.rampTimeDoubleSpinBox.setMinimum(0.500000000000000)

        self.gridLayout.addWidget(self.rampTimeDoubleSpinBox, 0, 1, 1, 1)

        self.label_3 = QLabel(self.groupBox_2)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 0, 0, 1, 1)

        self.label_4 = QLabel(self.groupBox_2)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout.addWidget(self.label_4, 1, 0, 1, 1)

        self.rampEndSpeedSpinBox = QSpinBox(self.groupBox_2)
        self.rampEndSpeedSpinBox.setObjectName(u"rampEndSpeedSpinBox")
        self.rampEndSpeedSpinBox.setMinimum(-100)
        self.rampEndSpeedSpinBox.setMaximum(100)

        self.gridLayout.addWidget(self.rampEndSpeedSpinBox, 1, 1, 1, 1)

        self.startRampPushButton = QPushButton(self.groupBox_2)
        self.startRampPushButton.setObjectName(u"startRampPushButton")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.MinimumExpanding)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.startRampPushButton.sizePolicy().hasHeightForWidth())
        self.startRampPushButton.setSizePolicy(sizePolicy)

        self.gridLayout.addWidget(self.startRampPushButton, 0, 2, 2, 1)


        self.gridLayout_3.addWidget(self.groupBox_2, 2, 1, 2, 1)

        self.groupBox_3 = QGroupBox(Form)
        self.groupBox_3.setObjectName(u"groupBox_3")
        self.horizontalLayout = QHBoxLayout(self.groupBox_3)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.label_2 = QLabel(self.groupBox_3)
        self.label_2.setObjectName(u"label_2")

        self.horizontalLayout.addWidget(self.label_2)

        self.brightnessHorizontalSlider = QSlider(self.groupBox_3)
        self.brightnessHorizontalSlider.setObjectName(u"brightnessHorizontalSlider")
        self.brightnessHorizontalSlider.setMaximum(100)
        self.brightnessHorizontalSlider.setOrientation(Qt.Horizontal)

        self.horizontalLayout.addWidget(self.brightnessHorizontalSlider)

        self.brightnessLcdNumber = QLCDNumber(self.groupBox_3)
        self.brightnessLcdNumber.setObjectName(u"brightnessLcdNumber")

        self.horizontalLayout.addWidget(self.brightnessLcdNumber)


        self.gridLayout_3.addWidget(self.groupBox_3, 1, 0, 1, 2)

        self.groupBox = QGroupBox(Form)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout_2 = QGridLayout(self.groupBox)
        self.gridLayout_2.setObjectName(u"gridLayout_2")
        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")

        self.gridLayout_2.addWidget(self.label, 0, 0, 1, 1)

        self.speedHorizontalSlider = QSlider(self.groupBox)
        self.speedHorizontalSlider.setObjectName(u"speedHorizontalSlider")
        self.speedHorizontalSlider.setMinimum(-100)
        self.speedHorizontalSlider.setMaximum(100)
        self.speedHorizontalSlider.setTracking(False)
        self.speedHorizontalSlider.setOrientation(Qt.Horizontal)

        self.gridLayout_2.addWidget(self.speedHorizontalSlider, 0, 1, 1, 1)

        self.lcdNumber = QLCDNumber(self.groupBox)
        self.lcdNumber.setObjectName(u"lcdNumber")

        self.gridLayout_2.addWidget(self.lcdNumber, 0, 2, 1, 1)


        self.gridLayout_3.addWidget(self.groupBox, 0, 0, 1, 2)

        self.horizontalLayout_3 = QHBoxLayout()
        self.horizontalLayout_3.setObjectName(u"horizontalLayout_3")
        self.stopTrainPushButton = QPushButton(Form)
        self.stopTrainPushButton.setObjectName(u"stopTrainPushButton")

        self.horizontalLayout_3.addWidget(self.stopTrainPushButton)

        self.lightsOffPushButton = QPushButton(Form)
        self.lightsOffPushButton.setObjectName(u"lightsOffPushButton")

        self.horizontalLayout_3.addWidget(self.lightsOffPushButton)


        self.gridLayout_3.addLayout(self.horizontalLayout_3, 3, 0, 1, 1)

        QWidget.setTabOrder(self.speedHorizontalSlider, self.brightnessHorizontalSlider)
        QWidget.setTabOrder(self.brightnessHorizontalSlider, self.toolButton)
        QWidget.setTabOrder(self.toolButton, self.toolButton_2)
        QWidget.setTabOrder(self.toolButton_2, self.toolButton_3)
        QWidget.setTabOrder(self.toolButton_3, self.toolButton_4)
        QWidget.setTabOrder(self.toolButton_4, self.toolButton_5)
        QWidget.setTabOrder(self.toolButton_5, self.toolButton_6)
        QWidget.setTabOrder(self.toolButton_6, self.toolButton_7)
        QWidget.setTabOrder(self.toolButton_7, self.toolButton_8)
        QWidget.setTabOrder(self.toolButton_8, self.toolButton_9)
        QWidget.setTabOrder(self.toolButton_9, self.toolButton_10)
        QWidget.setTabOrder(self.toolButton_10, self.toolButton_11)
        QWidget.setTabOrder(self.toolButton_11, self.rampTimeDoubleSpinBox)
        QWidget.setTabOrder(self.rampTimeDoubleSpinBox, self.startRampPushButton)
        QWidget.setTabOrder(self.startRampPushButton, self.rampEndSpeedSpinBox)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Lego vonat vez\u00e9rl\u00e9s", None))
        self.ledGroupBox.setTitle(QCoreApplication.translate("Form", u"LED sz\u00ednv\u00e1lt\u00e1s", None))
        self.toolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_2.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_3.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_4.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_5.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_6.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_7.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_8.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_9.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_10.setText(QCoreApplication.translate("Form", u"...", None))
        self.toolButton_11.setText(QCoreApplication.translate("Form", u"...", None))
        self.groupBox_2.setTitle(QCoreApplication.translate("Form", u"Gyors\u00edt/lass\u00edt", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Id\u0151 (m\u00e1sodperc):", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"C\u00e9lsebess\u00e9g:", None))
        self.startRampPushButton.setText(QCoreApplication.translate("Form", u"Mehet", None))
        self.groupBox_3.setTitle(QCoreApplication.translate("Form", u"F\u00e9nysz\u00f3r\u00f3", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"F\u00e9nyer\u0151:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Azonnali szab\u00e1lyoz\u00e1s", None))
        self.label.setText(QCoreApplication.translate("Form", u"Sebess\u00e9g:", None))
        self.stopTrainPushButton.setText(QCoreApplication.translate("Form", u"Vonat \u00e1llj!", None))
        self.lightsOffPushButton.setText(QCoreApplication.translate("Form", u"F\u00e9nysz\u00f3r\u00f3 kikapcsol\u00e1sa", None))
    # retranslateUi

