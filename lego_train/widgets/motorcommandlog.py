from typing import Any, Tuple, List, Optional

from PySide2 import QtCore, QtGui
from PySide2.QtCore import Signal
import time
import pickle


class MotorCommandLog(QtCore.QAbstractItemModel):
    motorCommandNeedsToBeIssued= Signal(str, int, object)
    _playback_started : Optional[float] = None
    _playback_timer: Optional[int] = None
    _playback_pointer: Optional[int] = None
    _logdata: List[Tuple[float, str, int, Tuple[Any, ...]]]
    playbackStarted = Signal(bool)
    def __init__(self):
        super().__init__(parent = None)
        self._logdata = []

    def rowCount(self, parent=None, *args, **kwargs):
        return len(self._logdata)

    def columnCount(self, parent=None, *args, **kwargs):
        return 4

    def data(self, index, role=None):
        print(f'{index.row()=}, {index.column()=}, {role=}')
        if (index.column() == 0) and (role == QtCore.Qt.DisplayRole):
            return f'{self._logdata[index.row()][0] - self._logdata[0][0]:.3f}'
        elif (index.column() == 1) and (role == QtCore.Qt.DisplayRole):
            return f'{self._logdata[index.row()][1]}'
        elif (index.column() == 2) and (role == QtCore.Qt.DisplayRole):
            return f'{"A" if self._logdata[index.row()][2]==0 else "B"}'
        elif (index.column() == 3) and (role == QtCore.Qt.DisplayRole):
            return ', '.join([str(x) for x in self._logdata[index.row()][3]])
        elif (role == QtCore.Qt.BackgroundRole):
            if (index.row() == self._playback_pointer) and self.is_playing():
                return QtGui.QColor('lightgreen')
        elif (index.column() == 0) and (role == QtCore.Qt.DecorationRole):
            if (index.row() == self._playback_pointer) and self.is_playing():
                return QtGui.QIcon.fromTheme('media-playback-start')
        else:
            return None

    def headerData(self, section, orientation, role=None):
        if (orientation == QtCore.Qt.Horizontal) and (role == QtCore.Qt.DisplayRole):
            return ['Idő (s)', 'Művelet', 'Motor', 'Paraméterek'][section]

    def parent(self, child):
        return QtCore.QModelIndex()

    def index(self, row, column, parent=None, *args, **kwargs):
        return self.createIndex(row, column, None)

    def flags(self, index):
        return QtCore.Qt.ItemNeverHasChildren | QtCore.Qt.ItemIsEnabled | QtCore.Qt.ItemIsSelectable

    def clear(self):
        self.beginResetModel()
        self._logdata = []
        self.endResetModel()

    def save(self, filename: str):
        with open(filename, 'wb') as f:
            pickle.dump(self._logdata, f)

    def append(self, cmd:str, motorindex:int, args: Tuple[Any, ...]):
        self.beginInsertRows(QtCore.QModelIndex(), len(self._logdata), len(self._logdata))
        self._logdata.append((time.monotonic(), cmd, motorindex, args))
        self.endInsertRows()

    def load(self, filename: str):
        self.beginResetModel()
        with open(filename, 'rb') as f:
            self._logdata = pickle.load(f)
        self.endResetModel()

    def playback_start(self):
        if self.is_playing():
            return

        self._playback_started = time.monotonic()
        self._playback_pointer = 0
        self._playback_timer = self.startTimer(3, QtCore.Qt.PreciseTimer)
        self.dataChanged.emit(self.index(self._playback_pointer, 0), self.index(self._playback_pointer, self.columnCount()))
        self.playbackStarted.emit(True)

    def playback_stop(self):
        if self.is_playing():
            self.killTimer(self._playback_timer)
            self._playback_started = None
            self._playback_timer = None
            self._playback_pointer = 0
            self.playbackStarted.emit(False)
            self.dataChanged.emit(self.index(0, 0), self.index(self.rowCount(), self.columnCount()))
        pass

    def is_playing(self):
        return (self._playback_timer is not None) or (self._playback_started is not None)

    def timerEvent(self, event):
        if not self.is_playing():
            assert False
        if self._playback_pointer >= len(self._logdata):
            self.playback_stop()
            return
        # see if the current command is due
        timestamp, command, motorindex, parameters = self._logdata[self._playback_pointer]
        if (time.monotonic() - self._playback_started) > (timestamp - self._logdata[0][0]) :
            # the current command is due
            self.motorCommandNeedsToBeIssued.emit(command, motorindex, parameters)
            self._playback_pointer += 1
            self.dataChanged.emit(self.index(self._playback_pointer-1, 0), self.index(self._playback_pointer, self.columnCount()))
