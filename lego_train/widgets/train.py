import queue

from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtCore import Slot
from .train_ui import Ui_Form
import bricknil
import bricknil.const
from ..asyncdrivers.train import Train, TrainCommand
import multiprocessing


class TrainControlWidget(QtWidgets.QWidget, Ui_Form):
    traincmdqueue: multiprocessing.Queue
    trainreplycurve: multiprocessing.Queue
    process: multiprocessing.Process

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(Form)
        layout: QtWidgets.QHBoxLayout = self.ledGroupBox.layout()
        print(f'{len(self.ledGroupBox.children())}')
        for i, toolbutton in enumerate(self.ledGroupBox.children()):
            print(f'Child {i}, {type(toolbutton)}')
            if not isinstance(toolbutton, QtWidgets.QToolButton):
                continue
            assert isinstance(toolbutton, QtWidgets.QToolButton)
            pal: QtGui.QPalette = toolbutton.palette()
            pal.setColor(QtGui.QPalette.Button, QtGui.QColor(bricknil.const.Color(i-1).name.replace('_','')))
            toolbutton.setPalette(pal)
            toolbutton.setText('')
            toolbutton.setToolTip(bricknil.const.Color(i-1).name)
            toolbutton.clicked.connect(self.onLed)
        self.traincmdqueue = multiprocessing.Queue()
        self.trainreplycurve = multiprocessing.Queue()
        self.process = multiprocessing.Process(target=self.subprocess, args=(self.traincmdqueue, self.trainreplycurve))
        self.process.start()
        self.speedHorizontalSlider.valueChanged.connect(self.onSpeedChanged)
        self.brightnessHorizontalSlider.valueChanged.connect(self.onBrightnessChanged)
        self.stopTrainPushButton.clicked.connect(self.onStop)
        self.startTimer(10, QtCore.Qt.PreciseTimer)
        self.setEnabled(False)
        self.startRampPushButton.clicked.connect(self.onRamp)
        self.lightsOffPushButton.clicked.connect(self.onLightsOff)

    @Slot(name='onLightsOff')
    def onLightsOff(self):
        self.traincmdqueue.put(TrainCommand('setbrightness', 0))

    @Slot(float, name='onSpeedChanged')
    def onSpeedChanged(self, value: float):
        self.traincmdqueue.put(TrainCommand('setspeed', int(value)))

    @Slot(float, name='onBrightnessChanged')
    def onBrightnessChanged(self, value: float):
        self.traincmdqueue.put(TrainCommand('setbrightness', int(value)))

    @Slot(name='onStop')
    def onStop(self):
        self.speedHorizontalSlider.setValue(0)

    @Slot(name='onLed')
    def onLed(self):
        for i, toolbutton in enumerate(self.ledGroupBox.children()):
            if self.sender() is toolbutton:
                self.traincmdqueue.put(TrainCommand('setled', bricknil.const.Color(i-1)))

    @staticmethod
    def subprocess(cmdqueue, replyqueue):
        async def system():
            train = Train('My train', cmdqueue, replyqueue)
        bricknil.start(system)

    def timerEvent(self, event):
        try:
            message = self.trainreplycurve.get_nowait()
        except queue.Empty:
            return
        assert isinstance(message, TrainCommand)
        if message.command == 'speedreply':
            self.lcdNumber.display(message.arguments[0])
            self.speedHorizontalSlider.blockSignals(True)
            self.speedHorizontalSlider.setValue(message.arguments[0])
            self.speedHorizontalSlider.blockSignals(False)
        elif message.command == 'online':
            self.setEnabled(True)
        elif message.command == 'brightnessreply':
            self.brightnessLcdNumber.display(message.arguments[0])
            self.brightnessHorizontalSlider.blockSignals(True)
            self.brightnessHorizontalSlider.setValue(message.arguments[0])
            self.brightnessHorizontalSlider.blockSignals(False)

    def closeEvent(self, event):
        self.traincmdqueue.put(TrainCommand('exit'))
        self.process.terminate()
        event.accept()

    @Slot(name='onRamp')
    def onRamp(self):
        self.traincmdqueue.put(TrainCommand('ramp', self.rampTimeDoubleSpinBox.value(), self.rampEndSpeedSpinBox.value()))