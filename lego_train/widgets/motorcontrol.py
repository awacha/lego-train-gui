import multiprocessing
import queue
import time

import bricknil
from PySide2 import QtWidgets, QtCore, QtGui
from PySide2.QtCore import Slot

from .motorcommandlog import MotorCommandLog
from .motorcontrol_ui import Ui_Form
from .motorsetup import MotorSetup
from ..asyncdrivers.motors import MotorsHub, MotorHubCommand
from ..asyncdrivers.remotecontrol import RemoteControl, RemoteCommand


class MotorControlWidget(QtWidgets.QWidget, Ui_Form):
    motorcmdqueue: multiprocessing.Queue
    motorreplyqueue: multiprocessing.Queue
    remotecmdqueue: multiprocessing.Queue
    remotereplyqueue: multiprocessing.Queue
    subprocess: multiprocessing.Process
    motor0setup: MotorSetup
    motor1setup: MotorSetup
    motorcommandlog: MotorCommandLog
    remote_online: bool = False
    motors_online: bool = False

    def __init__(self, parent=None):
        super().__init__(parent)
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(Form)
        self.motor0setup = MotorSetup(self.motorSettingsGroupBox)
        self.motor1setup = MotorSetup(self.motorSettingsGroupBox)
        self.motorSettingsGroupBox.setLayout(QtWidgets.QHBoxLayout())
        self.motorSettingsGroupBox.layout().addWidget(self.motor0setup)
        self.motorSettingsGroupBox.layout().addWidget(self.motor1setup)

        self.motorcmdqueue = multiprocessing.Queue()
        self.motorreplyqueue = multiprocessing.Queue()
        self.remotecmdqueue = multiprocessing.Queue()
        self.remotereplyqueue = multiprocessing.Queue()

        self.subprocess = multiprocessing.Process(target=runsubprocess, name='Motors&Remote subprocess',
                                                  args=(self.motorcmdqueue, self.motorreplyqueue, self.remotecmdqueue,
                                                        self.remotereplyqueue))
        self.motorcommandlog = MotorCommandLog()
        self.motorcommandlog.playbackStarted.connect(self.on_motorcommandlog_playbackStarted)
        self.motorcommandlog.motorCommandNeedsToBeIssued.connect(self.on_motorcommandlog_motorCommandNeedsToBeIssued)
        self.logTreeView.setModel(self.motorcommandlog)
        self.setEnabled(False)
        self.subprocess.start()
        self.startTimer(10, QtCore.Qt.PreciseTimer)
        self.motor0setup.motorCommandNeedsToBeIssued.connect(self.onMotorCommandNeedsToBeIssued)
        self.motor1setup.motorCommandNeedsToBeIssued.connect(self.onMotorCommandNeedsToBeIssued)

    def timerEvent(self, event):
        try:
            motorreply: MotorHubCommand = self.motorreplyqueue.get_nowait()
            #            self.motorreplyqueue.task_done()
#            if motorreply.command not in ['voltage', 'current']:
#                print(f'Motor reply: {motorreply.command}, {motorreply.arguments}')
        except queue.Empty:
            pass
        else:
            if not self.motors_online:
                self.motors_online = True
                if self.motors_online and self.remote_online:
                    self.setEnabled(True)
            if (motorreply.command == 'motor1') or (motorreply.command == 'motor0'):
                pos, speed = motorreply.arguments
                if isinstance(pos, int):
                    (self.motor0setup if motorreply.command == 'motor0' else self.motor1setup).updatePosition(pos)
                if isinstance(speed, int):
                    (self.motor0setup if motorreply.command == 'motor0' else self.motor1setup).updateSpeed(speed)
        try:
            remotereply: RemoteCommand = self.remotereplyqueue.get_nowait()
        except queue.Empty:
            pass
        else:
            if not self.remote_online:
                self.remote_online = True
                if self.motors_online and self.remote_online:
                    self.setEnabled(True)
            if (remotereply.command == 'leftbuttons') or (remotereply.command == 'rightbuttons'):
                plus, stop, minus = remotereply.arguments[0]
                (self.motor0setup if remotereply.command == 'leftbuttons' else self.motor1setup).updateButtonState(
                    bool(plus), bool(stop), bool(minus))

    def onMotorCommandNeedsToBeIssued(self, cmdname, args):
        motorindex = 0 if (self.sender() is self.motor0setup) else 1
        print(f'Sending command: {cmdname}, {motorindex=}, {args=}')
        self.motorcmdqueue.put_nowait(MotorHubCommand(cmdname, motorindex, *args))
        if self.logRecordToolButton.isChecked():
            self.motorcommandlog.append(cmdname, motorindex, args)

    @Slot()
    def on_motorAUpToolButton_pressed(self):
        self.motor0setup.updateButtonState(self.motorAUpToolButton.isDown(), self.motorAStopToolButton.isDown(),
                                           self.motorADownToolButton.isDown())

    @Slot()
    def on_motorAStopToolButton_pressed(self):
        self.motor0setup.updateButtonState(self.motorAUpToolButton.isDown(), self.motorAStopToolButton.isDown(),
                                           self.motorADownToolButton.isDown())

    @Slot()
    def on_motorADownToolButton_pressed(self):
        self.motor0setup.updateButtonState(self.motorAUpToolButton.isDown(), self.motorAStopToolButton.isDown(),
                                           self.motorADownToolButton.isDown())

    @Slot()
    def on_motorBUpToolButton_pressed(self):
        self.motor1setup.updateButtonState(self.motorBUpToolButton.isDown(), self.motorBStopToolButton.isDown(),
                                           self.motorBDownToolButton.isDown())

    @Slot()
    def on_motorBStopToolButton_pressed(self):
        self.motor1setup.updateButtonState(self.motorBUpToolButton.isDown(), self.motorBStopToolButton.isDown(),
                                           self.motorBDownToolButton.isDown())

    @Slot()
    def on_motorBDownToolButton_pressed(self):
        self.motor1setup.updateButtonState(self.motorBUpToolButton.isDown(), self.motorBStopToolButton.isDown(),
                                           self.motorBDownToolButton.isDown())

    @Slot()
    def on_logSaveToolButton_clicked(self):
        filename, fltr = QtWidgets.QFileDialog.getSaveFileName(self, "Napló mentése", '', 'Motor napló (*.motlog);; Minden file (*)', 'Motor napló (*.motlog)')
        if not filename:
            return
        self.motorcommandlog.save(filename)

    @Slot()
    def on_logLoadToolButton_clicked(self):
        filename, fltr = QtWidgets.QFileDialog.getOpenFileName(self, "Napló betöltése", '', 'Motor napló (*.motlog);; Minden file (*)', 'Motor napló (*.motlog)')
        if not filename:
            return
        self.motorcommandlog.load(filename)

    @Slot()
    def on_logReplayToolButton_toggled(self):
        if self.motorcommandlog.is_playing():
            self.motorcommandlog.playback_stop()
        else:
            self.motorcommandlog.playback_start()

    @Slot(bool)
    def on_motorcommandlog_playbackStarted(self, started: bool):
        if started:
            self.logRecordToolButton.setChecked(False)
        else:
            self.motorcmdqueue.put_nowait(MotorHubCommand('stop', 0))
            self.motorcmdqueue.put_nowait(MotorHubCommand('stop', 1))
        for w in [self.logRecordToolButton, self.logSaveToolButton, self.logLoadToolButton, self.logClearToolButton, self.motorAUpToolButton, self.motorAStopToolButton, self.motorADownToolButton, self.motorBUpToolButton, self.motorBStopToolButton, self.motorBDownToolButton]:
            w.setEnabled(not started)
        self.logReplayToolButton.setIcon(QtGui.QIcon.fromTheme("media-playback-stop" if started else "media-playback-start"))

    @Slot()
    def on_logClearToolButton_clicked(self):
        self.motorcommandlog.clear()

    def closeEvent(self, event):
        self.remotecmdqueue.put_nowait(RemoteCommand('exit'))
        self.motorcmdqueue.put_nowait(MotorHubCommand('exit'))
        time.sleep(1)
        self.subprocess.terminate()
        return super().closeEvent(event)

    @Slot()
    def on_motorcommandlog_motorCommandNeedsToBeIssued(self, cmd, motor, args):
        if self.motorcommandlog.is_playing():
            self.motorcmdqueue.put_nowait(MotorHubCommand(cmd, motor, *args))


def runsubprocess(cmdqueuemotors, replyqueuemotors, cmdqueueremote, replyqueueremote):
    async def system():
        hub = MotorsHub('Motors', cmdqueuemotors, replyqueuemotors)
        remote = RemoteControl('Remote', cmdqueueremote, replyqueueremote)

    bricknil.start(system)
