# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'motorsetup.ui'
##
## Created by: Qt User Interface Compiler version 5.15.6
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *  # type: ignore
from PySide2.QtGui import *  # type: ignore
from PySide2.QtWidgets import *  # type: ignore


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(347, 469)
        self.gridLayout_5 = QGridLayout(Form)
        self.gridLayout_5.setObjectName(u"gridLayout_5")
        self.constantSpeedSpinBox = QSpinBox(Form)
        self.constantSpeedSpinBox.setObjectName(u"constantSpeedSpinBox")
        self.constantSpeedSpinBox.setMinimum(-100)
        self.constantSpeedSpinBox.setMaximum(100)
        self.constantSpeedSpinBox.setValue(50)

        self.gridLayout_5.addWidget(self.constantSpeedSpinBox, 1, 1, 1, 1)

        self.label = QLabel(Form)
        self.label.setObjectName(u"label")

        self.gridLayout_5.addWidget(self.label, 0, 0, 1, 1)

        self.label_4 = QLabel(Form)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout_5.addWidget(self.label_4, 3, 0, 1, 1)

        self.maxSpeedSpinBox = QSpinBox(Form)
        self.maxSpeedSpinBox.setObjectName(u"maxSpeedSpinBox")
        self.maxSpeedSpinBox.setMaximum(100)
        self.maxSpeedSpinBox.setValue(100)

        self.gridLayout_5.addWidget(self.maxSpeedSpinBox, 2, 1, 1, 1)

        self.driveMethodComboBox = QComboBox(Form)
        self.driveMethodComboBox.addItem("")
        self.driveMethodComboBox.addItem("")
        self.driveMethodComboBox.addItem("")
        self.driveMethodComboBox.addItem("")
        self.driveMethodComboBox.setObjectName(u"driveMethodComboBox")

        self.gridLayout_5.addWidget(self.driveMethodComboBox, 0, 1, 1, 1)

        self.incrementSpeedStepSpinBox = QSpinBox(Form)
        self.incrementSpeedStepSpinBox.setObjectName(u"incrementSpeedStepSpinBox")
        self.incrementSpeedStepSpinBox.setMinimum(1)
        self.incrementSpeedStepSpinBox.setMaximum(100)
        self.incrementSpeedStepSpinBox.setValue(10)

        self.gridLayout_5.addWidget(self.incrementSpeedStepSpinBox, 3, 1, 1, 1)

        self.label_2 = QLabel(Form)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout_5.addWidget(self.label_2, 1, 0, 1, 1)

        self.label_3 = QLabel(Form)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout_5.addWidget(self.label_3, 2, 0, 1, 1)

        self.label_6 = QLabel(Form)
        self.label_6.setObjectName(u"label_6")

        self.gridLayout_5.addWidget(self.label_6, 10, 0, 1, 1)

        self.label_11 = QLabel(Form)
        self.label_11.setObjectName(u"label_11")

        self.gridLayout_5.addWidget(self.label_11, 11, 0, 1, 1)

        self.accelerationDoubleSpinBox = QDoubleSpinBox(Form)
        self.accelerationDoubleSpinBox.setObjectName(u"accelerationDoubleSpinBox")
        self.accelerationDoubleSpinBox.setMaximum(99999.000000000000000)

        self.gridLayout_5.addWidget(self.accelerationDoubleSpinBox, 4, 1, 1, 1)

        self.label_5 = QLabel(Form)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout_5.addWidget(self.label_5, 4, 0, 1, 1)

        self.groupBox = QGroupBox(Form)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.fetchIntervalLeftToolButton = QToolButton(self.groupBox)
        self.fetchIntervalLeftToolButton.setObjectName(u"fetchIntervalLeftToolButton")

        self.gridLayout.addWidget(self.fetchIntervalLeftToolButton, 0, 2, 1, 1)

        self.intervalWidthSpinBox = QSpinBox(self.groupBox)
        self.intervalWidthSpinBox.setObjectName(u"intervalWidthSpinBox")

        self.gridLayout.addWidget(self.intervalWidthSpinBox, 4, 1, 1, 1)

        self.label_9 = QLabel(self.groupBox)
        self.label_9.setObjectName(u"label_9")

        self.gridLayout.addWidget(self.label_9, 3, 0, 1, 1)

        self.fetchIntervalCenterToolButton = QToolButton(self.groupBox)
        self.fetchIntervalCenterToolButton.setObjectName(u"fetchIntervalCenterToolButton")

        self.gridLayout.addWidget(self.fetchIntervalCenterToolButton, 3, 2, 1, 1)

        self.intervalCenterSpinBox = QSpinBox(self.groupBox)
        self.intervalCenterSpinBox.setObjectName(u"intervalCenterSpinBox")
        self.intervalCenterSpinBox.setWrapping(True)

        self.gridLayout.addWidget(self.intervalCenterSpinBox, 3, 1, 1, 1)

        self.label_10 = QLabel(self.groupBox)
        self.label_10.setObjectName(u"label_10")

        self.gridLayout.addWidget(self.label_10, 4, 0, 1, 1)

        self.label_8 = QLabel(self.groupBox)
        self.label_8.setObjectName(u"label_8")

        self.gridLayout.addWidget(self.label_8, 2, 0, 1, 1)

        self.fetchIntervalRightToolButton = QToolButton(self.groupBox)
        self.fetchIntervalRightToolButton.setObjectName(u"fetchIntervalRightToolButton")

        self.gridLayout.addWidget(self.fetchIntervalRightToolButton, 2, 2, 1, 1)

        self.label_7 = QLabel(self.groupBox)
        self.label_7.setObjectName(u"label_7")

        self.gridLayout.addWidget(self.label_7, 0, 0, 1, 1)

        self.intervalLeftSpinBox = QSpinBox(self.groupBox)
        self.intervalLeftSpinBox.setObjectName(u"intervalLeftSpinBox")
        self.intervalLeftSpinBox.setWrapping(True)
        self.intervalLeftSpinBox.setMinimum(-999999999)

        self.gridLayout.addWidget(self.intervalLeftSpinBox, 0, 1, 1, 1)

        self.intervalRightSpinBox = QSpinBox(self.groupBox)
        self.intervalRightSpinBox.setObjectName(u"intervalRightSpinBox")
        self.intervalRightSpinBox.setWrapping(True)

        self.gridLayout.addWidget(self.intervalRightSpinBox, 2, 1, 1, 1)


        self.gridLayout_5.addWidget(self.groupBox, 5, 0, 1, 2)

        self.currentPositionLabel = QLabel(Form)
        self.currentPositionLabel.setObjectName(u"currentPositionLabel")

        self.gridLayout_5.addWidget(self.currentPositionLabel, 10, 1, 1, 1)

        self.currentSpeedLabel = QLabel(Form)
        self.currentSpeedLabel.setObjectName(u"currentSpeedLabel")

        self.gridLayout_5.addWidget(self.currentSpeedLabel, 11, 1, 1, 1)

        QWidget.setTabOrder(self.driveMethodComboBox, self.constantSpeedSpinBox)
        QWidget.setTabOrder(self.constantSpeedSpinBox, self.maxSpeedSpinBox)
        QWidget.setTabOrder(self.maxSpeedSpinBox, self.incrementSpeedStepSpinBox)
        QWidget.setTabOrder(self.incrementSpeedStepSpinBox, self.accelerationDoubleSpinBox)
        QWidget.setTabOrder(self.accelerationDoubleSpinBox, self.intervalLeftSpinBox)
        QWidget.setTabOrder(self.intervalLeftSpinBox, self.fetchIntervalLeftToolButton)
        QWidget.setTabOrder(self.fetchIntervalLeftToolButton, self.intervalRightSpinBox)
        QWidget.setTabOrder(self.intervalRightSpinBox, self.fetchIntervalRightToolButton)
        QWidget.setTabOrder(self.fetchIntervalRightToolButton, self.intervalCenterSpinBox)
        QWidget.setTabOrder(self.intervalCenterSpinBox, self.fetchIntervalCenterToolButton)
        QWidget.setTabOrder(self.fetchIntervalCenterToolButton, self.intervalWidthSpinBox)

        self.retranslateUi(Form)

        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.label.setText(QCoreApplication.translate("Form", u"Vez\u00e9rl\u00e9s t\u00edpusa:", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Sebess\u00e9g l\u00e9ptet\u00e9s:", None))
        self.driveMethodComboBox.setItemText(0, QCoreApplication.translate("Form", u"Nyom\u00e1sra \u00e1lland\u00f3 sebess\u00e9g", None))
        self.driveMethodComboBox.setItemText(1, QCoreApplication.translate("Form", u"Nyom\u00e1sra gyors\u00edt/lass\u00edt", None))
        self.driveMethodComboBox.setItemText(2, QCoreApplication.translate("Form", u"Folyamatos nyom\u00e1sra gyorsul", None))
        self.driveMethodComboBox.setItemText(3, QCoreApplication.translate("Form", u"Hat\u00e1rok k\u00f6z\u00f6tt forog", None))

        self.label_2.setText(QCoreApplication.translate("Form", u"Sebess\u00e9g:", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Maximum sebess\u00e9g:", None))
        self.label_6.setText(QCoreApplication.translate("Form", u"Jelenlegi hely:", None))
        self.label_11.setText(QCoreApplication.translate("Form", u"Jelenlegi sebess\u00e9g:", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Gyorsul\u00e1s:", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Intervallum", None))
        self.fetchIntervalLeftToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.label_9.setText(QCoreApplication.translate("Form", u"K\u00f6zepe:", None))
        self.fetchIntervalCenterToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.label_10.setText(QCoreApplication.translate("Form", u"Sz\u00e9less\u00e9ge:", None))
        self.label_8.setText(QCoreApplication.translate("Form", u"Jobb sz\u00e9le:", None))
        self.fetchIntervalRightToolButton.setText(QCoreApplication.translate("Form", u"...", None))
        self.label_7.setText(QCoreApplication.translate("Form", u"Bal sz\u00e9le:", None))
        self.currentPositionLabel.setText(QCoreApplication.translate("Form", u"TextLabel", None))
        self.currentSpeedLabel.setText(QCoreApplication.translate("Form", u"TextLabel", None))
    # retranslateUi

