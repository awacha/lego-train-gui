import queue
import enum
import sys

from PySide2 import QtWidgets, QtCore
from PySide2.QtCore import Slot
from .motor_hacking_ui import Ui_Form
import bricknil
import bricknil.const
from ..asyncdrivers.motors import MotorsHub, MotorHubCommand
import multiprocessing


class MotorCommands(enum.Enum):
    SetPosition = 'Abszolút pozícionálás'
    Rotate = 'Relatív pozícionálás'
    SetSpeed = 'Sebesség megadása'
    SetRamp = 'Gyorsítás/lassítás'


class MotorHackingWidget(QtWidgets.QWidget, Ui_Form):
    cmdqueue: multiprocessing.Queue
    replyqueue: multiprocessing.Queue
    process: multiprocessing.Process

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(Form)
        self.setEnabled(False)
        self.commandComboBox.addItems([e.value for e in MotorCommands])
        self.cmdqueue = multiprocessing.Queue()
        self.replyqueue = multiprocessing.Queue()
        self.process = multiprocessing.Process(target=self.subprocess, args=(self.cmdqueue, self.replyqueue))
        self.process.start()
        self.startTimer(10, QtCore.Qt.PreciseTimer)

    @Slot(str)
    def on_commandComboBox_currentIndexChanged(self, currentText):
        try:
            cmd = MotorCommands(currentText)
        except ValueError:
            return
        if cmd == MotorCommands.SetPosition:
            self.speedSpinBox.setEnabled(True)
            self.speedSpinBox.setRange(0, 100)
            self.speedSpinBox.setValue(50)
            self.positionSpinBox.setEnabled(True)
            self.positionSpinBox.setRange(-2**16, 2**16-1)
            self.positionSpinBox.setValue(0)
            self.maxPowerSpinBox.setEnabled(True)
            self.maxPowerSpinBox.setRange(0,100)
            self.maxPowerSpinBox.setValue(50)
            self.rampTimeDoubleSpinBox.setEnabled(False)
            self.rampTimeDoubleSpinBox.setRange(0,0)
            self.rampTimeDoubleSpinBox.setValue(0)
        elif cmd == MotorCommands.Rotate:
            self.speedSpinBox.setEnabled(True)
            self.speedSpinBox.setRange(-100, 100)
            self.speedSpinBox.setValue(0)
            self.positionSpinBox.setEnabled(True)
            self.positionSpinBox.setRange(0, 1000000)
            self.positionSpinBox.setValue(0)
            self.maxPowerSpinBox.setEnabled(True)
            self.maxPowerSpinBox.setRange(0, 100)
            self.maxPowerSpinBox.setValue(50)
            self.rampTimeDoubleSpinBox.setEnabled(False)
            self.rampTimeDoubleSpinBox.setRange(0, 0)
            self.rampTimeDoubleSpinBox.setValue(0)
        elif cmd == MotorCommands.SetSpeed:
            self.speedSpinBox.setEnabled(True)
            self.speedSpinBox.setRange(-100, 127)
            self.speedSpinBox.setValue(0)
            self.positionSpinBox.setEnabled(False)
            self.positionSpinBox.setRange(0, 0)
            self.positionSpinBox.setValue(0)
            self.maxPowerSpinBox.setEnabled(False)
            self.maxPowerSpinBox.setRange(0, 0)
            self.maxPowerSpinBox.setValue(0)
            self.rampTimeDoubleSpinBox.setEnabled(False)
            self.rampTimeDoubleSpinBox.setRange(0, 0)
            self.rampTimeDoubleSpinBox.setValue(0)
        elif cmd == MotorCommands.SetRamp:
            self.speedSpinBox.setEnabled(True)
            self.speedSpinBox.setRange(-100, 100)
            self.speedSpinBox.setValue(0)
            self.positionSpinBox.setEnabled(False)
            self.positionSpinBox.setRange(0, 0)
            self.positionSpinBox.setValue(0)
            self.maxPowerSpinBox.setEnabled(False)
            self.maxPowerSpinBox.setRange(0, 0)
            self.maxPowerSpinBox.setValue(0)
            self.rampTimeDoubleSpinBox.setEnabled(True)
            self.rampTimeDoubleSpinBox.setRange(0, 300)
            self.rampTimeDoubleSpinBox.setValue(1)

    @Slot(bool)
    def on_executeCommandToolButton_clicked(self, checked:bool):
        try:
            cmd = MotorCommands(self.commandComboBox.currentText())
        except ValueError:
            return
        if cmd == MotorCommands.Rotate:
            self.cmdqueue.put(MotorHubCommand(
                'rotate', 0 if self.motorARadioButton.isChecked() else 1,
                self.positionSpinBox.value(), self.speedSpinBox.value(), self.maxPowerSpinBox.value()
            ))
        elif cmd == MotorCommands.SetRamp:
            self.cmdqueue.put(MotorHubCommand(
                'ramp_speed', 0 if self.motorARadioButton.isChecked() else 1,
                self.speedSpinBox.value(), int(self.rampTimeDoubleSpinBox.value()*1000)
            ))
        elif cmd == MotorCommands.SetSpeed:
            self.cmdqueue.put(MotorHubCommand(
                'set_speed', 0 if self.motorARadioButton.isChecked() else 1,
                self.speedSpinBox.value()
            ))
        elif cmd == MotorCommands.SetPosition:
            self.cmdqueue.put(MotorHubCommand(
                'set_pos', 0 if self.motorARadioButton.isChecked() else 1,
                self.positionSpinBox.value(), self.speedSpinBox.value(), self.maxPowerSpinBox.value()
            ))

    @Slot(bool)
    def on_stopPushButton_clicked(self, checked:bool):
        self.cmdqueue.put(MotorHubCommand(
            'set_speed', 0, 0
        ))
        self.cmdqueue.put(MotorHubCommand(
            'set_speed', 1, 0
        ))

    @staticmethod
    def subprocess(cmdqueue, replyqueue):
        async def system():
            train = MotorsHub('Lego motors', cmdqueue, replyqueue)
        bricknil.start(system)

    def timerEvent(self, event):
        try:
            message = self.replyqueue.get_nowait()
        except queue.Empty:
            return
        assert isinstance(message, MotorHubCommand)
        if message.command == 'online':
            self.setEnabled(True)


    def closeEvent(self, event):
        self.cmdqueue.put(MotorHubCommand('exit'))
        self.process.terminate()
        event.accept()

