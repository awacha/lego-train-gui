# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'motor_hacking.ui'
##
## Created by: Qt User Interface Compiler version 5.15.6
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide2.QtCore import *  # type: ignore
from PySide2.QtGui import *  # type: ignore
from PySide2.QtWidgets import *  # type: ignore


class Ui_Form(object):
    def setupUi(self, Form):
        if not Form.objectName():
            Form.setObjectName(u"Form")
        Form.resize(400, 300)
        self.verticalLayout = QVBoxLayout(Form)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.groupBox = QGroupBox(Form)
        self.groupBox.setObjectName(u"groupBox")
        self.gridLayout = QGridLayout(self.groupBox)
        self.gridLayout.setObjectName(u"gridLayout")
        self.positionSpinBox = QSpinBox(self.groupBox)
        self.positionSpinBox.setObjectName(u"positionSpinBox")

        self.gridLayout.addWidget(self.positionSpinBox, 4, 1, 1, 1)

        self.maxPowerSpinBox = QSpinBox(self.groupBox)
        self.maxPowerSpinBox.setObjectName(u"maxPowerSpinBox")

        self.gridLayout.addWidget(self.maxPowerSpinBox, 6, 1, 1, 1)

        self.rampTimeDoubleSpinBox = QDoubleSpinBox(self.groupBox)
        self.rampTimeDoubleSpinBox.setObjectName(u"rampTimeDoubleSpinBox")

        self.gridLayout.addWidget(self.rampTimeDoubleSpinBox, 7, 1, 1, 1)

        self.label = QLabel(self.groupBox)
        self.label.setObjectName(u"label")

        self.gridLayout.addWidget(self.label, 4, 0, 1, 1)

        self.horizontalLayout = QHBoxLayout()
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.motorARadioButton = QRadioButton(self.groupBox)
        self.motorARadioButton.setObjectName(u"motorARadioButton")
        sizePolicy = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.motorARadioButton.sizePolicy().hasHeightForWidth())
        self.motorARadioButton.setSizePolicy(sizePolicy)
        self.motorARadioButton.setChecked(True)

        self.horizontalLayout.addWidget(self.motorARadioButton)

        self.motorBRadioButton = QRadioButton(self.groupBox)
        self.motorBRadioButton.setObjectName(u"motorBRadioButton")
        sizePolicy.setHeightForWidth(self.motorBRadioButton.sizePolicy().hasHeightForWidth())
        self.motorBRadioButton.setSizePolicy(sizePolicy)

        self.horizontalLayout.addWidget(self.motorBRadioButton)

        self.stopPushButton = QPushButton(self.groupBox)
        self.stopPushButton.setObjectName(u"stopPushButton")
        sizePolicy1 = QSizePolicy(QSizePolicy.Minimum, QSizePolicy.Preferred)
        sizePolicy1.setHorizontalStretch(0)
        sizePolicy1.setVerticalStretch(0)
        sizePolicy1.setHeightForWidth(self.stopPushButton.sizePolicy().hasHeightForWidth())
        self.stopPushButton.setSizePolicy(sizePolicy1)
        icon = QIcon(QIcon.fromTheme(u"process-stop"))
        self.stopPushButton.setIcon(icon)
        self.stopPushButton.setAutoDefault(True)

        self.horizontalLayout.addWidget(self.stopPushButton)


        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 2)

        self.label_5 = QLabel(self.groupBox)
        self.label_5.setObjectName(u"label_5")

        self.gridLayout.addWidget(self.label_5, 2, 0, 1, 1)

        self.label_4 = QLabel(self.groupBox)
        self.label_4.setObjectName(u"label_4")

        self.gridLayout.addWidget(self.label_4, 6, 0, 1, 1)

        self.label_3 = QLabel(self.groupBox)
        self.label_3.setObjectName(u"label_3")

        self.gridLayout.addWidget(self.label_3, 7, 0, 1, 1)

        self.speedSpinBox = QSpinBox(self.groupBox)
        self.speedSpinBox.setObjectName(u"speedSpinBox")

        self.gridLayout.addWidget(self.speedSpinBox, 5, 1, 1, 1)

        self.label_2 = QLabel(self.groupBox)
        self.label_2.setObjectName(u"label_2")

        self.gridLayout.addWidget(self.label_2, 5, 0, 1, 1)

        self.horizontalLayout_2 = QHBoxLayout()
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.commandComboBox = QComboBox(self.groupBox)
        self.commandComboBox.setObjectName(u"commandComboBox")
        sizePolicy2 = QSizePolicy(QSizePolicy.Expanding, QSizePolicy.Fixed)
        sizePolicy2.setHorizontalStretch(1)
        sizePolicy2.setVerticalStretch(0)
        sizePolicy2.setHeightForWidth(self.commandComboBox.sizePolicy().hasHeightForWidth())
        self.commandComboBox.setSizePolicy(sizePolicy2)

        self.horizontalLayout_2.addWidget(self.commandComboBox)

        self.executeCommandToolButton = QToolButton(self.groupBox)
        self.executeCommandToolButton.setObjectName(u"executeCommandToolButton")
        icon1 = QIcon(QIcon.fromTheme(u"system-run"))
        self.executeCommandToolButton.setIcon(icon1)

        self.horizontalLayout_2.addWidget(self.executeCommandToolButton)


        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 1, 1, 1)


        self.verticalLayout.addWidget(self.groupBox)


        self.retranslateUi(Form)

        self.stopPushButton.setDefault(True)


        QMetaObject.connectSlotsByName(Form)
    # setupUi

    def retranslateUi(self, Form):
        Form.setWindowTitle(QCoreApplication.translate("Form", u"Form", None))
        self.groupBox.setTitle(QCoreApplication.translate("Form", u"Parancsok", None))
        self.label.setText(QCoreApplication.translate("Form", u"Poz\u00edci\u00f3:", None))
        self.motorARadioButton.setText(QCoreApplication.translate("Form", u"\"A\" motor", None))
        self.motorBRadioButton.setText(QCoreApplication.translate("Form", u"\"B\" motor", None))
        self.stopPushButton.setText(QCoreApplication.translate("Form", u"Stop", None))
        self.label_5.setText(QCoreApplication.translate("Form", u"Parancs:", None))
        self.label_4.setText(QCoreApplication.translate("Form", u"Max. teljes\u00edtm\u00e9ny:", None))
        self.label_3.setText(QCoreApplication.translate("Form", u"Id\u0151:", None))
        self.label_2.setText(QCoreApplication.translate("Form", u"Sebess\u00e9g:", None))
        self.executeCommandToolButton.setText(QCoreApplication.translate("Form", u"Execute", None))
    # retranslateUi

