from typing import Optional
import time

from PySide2 import QtCore, QtWidgets
from PySide2.QtCore import Slot, Signal
from .motorsetup_ui import Ui_Form
import enum


class MotorControlMethod(enum.Enum):
    ConstantSpeedOnPress = 0
    IncrementSpeedOnPress = 1
    ConstantAccelerationOnPress = 2
    Interval = 3


class MotorSettings:
    method: MotorControlMethod
    constantspeed: float
    speedincrement: float
    maxspeedforincrement: float
    def __init__(self, **kwargs):
        for key, value in kwargs.items():
            setattr(self, key, value)



class MotorSetup(QtWidgets.QWidget, Ui_Form):
    currentspeed: int
    currentposition: int
    motorCommandNeedsToBeIssued= Signal(str, object)
    currentsetspeed: int = 0
    currentsetposition: int = 0
    plusbuttonpressedat: Optional[float] = None
    minusbuttonpressedat: Optional[float] = None
    interval_motion_bufferdistance: int = 5

    def __init__(self, parent):
        super().__init__(parent)
        self.setupUi(self)

    def setupUi(self, Form):
        super().setupUi(Form)
        self.intervalLeftSpinBox.setRange(-(1<<31), 1<<31-1)
        self.intervalRightSpinBox.setRange(-(1<<31), 1<<31-1)
        self.intervalCenterSpinBox.setRange(-(1<<31), 1<<31-1)
        self.intervalWidthSpinBox.setRange(0, 1<<31-1)
        self.startTimer(100, QtCore.Qt.PreciseTimer)

    def controlMethod(self) -> MotorControlMethod:
        return MotorControlMethod(self.driveMethodComboBox.currentIndex())

    @Slot(int)
    def on_intervalLeftSpinBox_valueChanged(self, value: int):
        left = value
        self.intervalRightSpinBox.setMinimum(value)
        right = self.intervalRightSpinBox.value()
        self.intervalCenterSpinBox.blockSignals(True)
        self.intervalCenterSpinBox.setValue(int(0.5*(left + right)))
        self.intervalCenterSpinBox.blockSignals(False)
        self.intervalWidthSpinBox.blockSignals(True)
        self.intervalWidthSpinBox.setValue(abs(right-left))
        self.intervalWidthSpinBox.blockSignals(False)

    @Slot(int)
    def on_intervalRightSpinBox_valueChanged(self, value: int):
        right = value
        self.intervalLeftSpinBox.setMaximum(value)
        left = self.intervalLeftSpinBox.value()
        self.intervalCenterSpinBox.blockSignals(True)
        self.intervalCenterSpinBox.setValue(int(0.5*(left + right)))
        self.intervalCenterSpinBox.blockSignals(False)
        self.intervalWidthSpinBox.blockSignals(True)
        self.intervalWidthSpinBox.setValue(abs(right-left))
        self.intervalWidthSpinBox.blockSignals(False)

    @Slot(int)
    def on_intervalCenterSpinBox_valueChanged(self, value: int):
        center = value
        width = self.intervalWidthSpinBox.value()
        self.intervalLeftSpinBox.blockSignals(True)
        self.intervalLeftSpinBox.setValue(int(center - 0.5*width))
        self.intervalLeftSpinBox.blockSignals(False)
        self.intervalRightSpinBox.blockSignals(True)
        self.intervalRightSpinBox.setValue(int(center + 0.5*width))
        self.intervalRightSpinBox.blockSignals(False)

    @Slot(int)
    def on_intervalWidthSpinBox_valueChanged(self, value: int):
        center = self.intervalCenterSpinBox.value()
        width = value
        self.intervalLeftSpinBox.blockSignals(True)
        self.intervalLeftSpinBox.setValue(int(center - 0.5*width))
        self.intervalLeftSpinBox.blockSignals(False)
        self.intervalRightSpinBox.blockSignals(True)
        self.intervalRightSpinBox.setValue(int(center + 0.5*width))
        self.intervalRightSpinBox.blockSignals(False)

    def updateSpeed(self, value: int):
        self.currentSpeedLabel.setText(f'{value:.0f}')
        self.currentspeed = value

    def updatePosition(self, value: int):
        self.currentPositionLabel.setText(f'{value:.0f}')
        self.currentposition = value
        if (self.controlMethod() == MotorControlMethod.Interval) and (
                ((self.currentposition <= self.intervalLeftSpinBox.value()) and self.currentsetspeed < 0) or
                ((self.currentposition >= self.intervalRightSpinBox.value()) and self.currentsetspeed > 0)):
            print(f'EMERGENCY STOP, {self.currentspeed=}, {self.currentsetspeed=}, {self.currentposition=}, {self.intervalLeftSpinBox.value()=}, {self.intervalRightSpinBox.value()=}')
            self.motorCommandNeedsToBeIssued.emit('stop', ())
            self.currentsetspeed = 0

    @Slot(bool)
    def on_fetchIntervalLeftToolButton_clicked(self, checked: bool):
        self.intervalLeftSpinBox.setValue(self.currentposition)

    @Slot(bool)
    def on_fetchIntervalRightToolButton_clicked(self, checked: bool):
        self.intervalRightSpinBox.setValue(self.currentposition)

    @Slot(bool)
    def on_fetchIntervalCenterToolButton_clicked(self, checked: bool):
        self.intervalCenterSpinBox.setValue(self.currentposition)

    def updateButtonState(self, plus: bool, stop: bool, minus: bool):
#        print(f'updateButtonState({plus}, {stop}, {minus}')
        # handle first button presses: holding down buttons will be handled via a timerEvent
        controlmethod = self.controlMethod()
        if stop:
            self.motorCommandNeedsToBeIssued.emit('stop', ())
            self.plusbuttonpressedat = None
            self.minusbuttonpressedat = None
            self.currentsetspeed = 0
            self.currentspeed = 0
            return
        if plus and (self.plusbuttonpressedat is None):
            self.plusbuttonpressedat = time.monotonic()
            if controlmethod == MotorControlMethod.IncrementSpeedOnPress:
                self.currentsetspeed += self.incrementSpeedStepSpinBox.value()
                self.currentsetspeed = min(self.maxSpeedSpinBox.value(), self.currentsetspeed)
                self.motorCommandNeedsToBeIssued.emit('set_speed', (self.currentsetspeed,))
            elif controlmethod == MotorControlMethod.Interval:
                if self.currentposition < self.intervalRightSpinBox.value():
                    self.currentsetspeed = self.constantSpeedSpinBox.value()

                    self.motorCommandNeedsToBeIssued.emit('set_speed', (self.currentsetspeed,))
            elif controlmethod == MotorControlMethod.ConstantSpeedOnPress:
                if self.accelerationDoubleSpinBox.value() == 0:
                    self.motorCommandNeedsToBeIssued.emit('set_speed', (self.constantSpeedSpinBox.value(),))
                else:
                    self.motorCommandNeedsToBeIssued.emit('ramp_speed', (self.constantSpeedSpinBox.value(), int(self.constantSpeedSpinBox.value()/self.accelerationDoubleSpinBox.value()*1000)))
            elif controlmethod == MotorControlMethod.ConstantAccelerationOnPress:
                self.currentsetspeed = 0
        if (not plus) and (self.plusbuttonpressedat is not None):
            self.plusbuttonpressedat = None
            if controlmethod == MotorControlMethod.IncrementSpeedOnPress:
                pass
            elif controlmethod == MotorControlMethod.Interval:
                self.motorCommandNeedsToBeIssued.emit('stop', ())
                self.currentsetspeed = 0
            elif controlmethod == MotorControlMethod.ConstantSpeedOnPress:
                self.motorCommandNeedsToBeIssued.emit('stop', ())
                self.currentsetspeed = 0
            elif controlmethod == MotorControlMethod.ConstantAccelerationOnPress:
                pass
        if minus and (self.minusbuttonpressedat is None):
            self.minusbuttonpressedat = time.monotonic()
            if controlmethod == MotorControlMethod.IncrementSpeedOnPress:
                self.currentsetspeed -= self.incrementSpeedStepSpinBox.value()
                self.currentsetspeed = max(-self.maxSpeedSpinBox.value(), self.currentsetspeed)
                self.motorCommandNeedsToBeIssued.emit('set_speed', (self.currentsetspeed,))
            elif controlmethod == MotorControlMethod.Interval:
                if self.currentposition > self.intervalLeftSpinBox.value():
                    self.currentsetspeed = -self.constantSpeedSpinBox.value()
                    self.motorCommandNeedsToBeIssued.emit('set_speed', (-self.constantSpeedSpinBox.value(),))
            elif controlmethod == MotorControlMethod.ConstantSpeedOnPress:
                if self.accelerationDoubleSpinBox.value() == 0:
                    self.motorCommandNeedsToBeIssued.emit('set_speed', (-self.constantSpeedSpinBox.value(),))
                else:
                    self.motorCommandNeedsToBeIssued.emit('ramp_speed', (-self.constantSpeedSpinBox.value(), int(self.constantSpeedSpinBox.value()/self.accelerationDoubleSpinBox.value()*1000)))
            elif controlmethod == MotorControlMethod.ConstantAccelerationOnPress:
                self.currentsetspeed = self.currentspeed
        if (not minus) and (self.minusbuttonpressedat is not None):
            self.minusbuttonpressedat = None
            if controlmethod == MotorControlMethod.IncrementSpeedOnPress:
                pass
            elif controlmethod == MotorControlMethod.Interval:
                self.motorCommandNeedsToBeIssued.emit('stop', ())
                self.currentsetspeed = 0
            elif controlmethod == MotorControlMethod.ConstantSpeedOnPress:
                self.motorCommandNeedsToBeIssued.emit('stop', ())
                self.currentsetspeed = 0
            elif controlmethod == MotorControlMethod.ConstantAccelerationOnPress:
                pass

    def timerEvent(self, event):
        # handle continuous button presses
        controlmethod = self.controlMethod()
        if self.plusbuttonpressedat is not None:
            deltat = time.monotonic() - self.plusbuttonpressedat
            if controlmethod == MotorControlMethod.IncrementSpeedOnPress:
                pass
            elif controlmethod == MotorControlMethod.Interval:
#                self.motorCommandNeedsToBeIssued.emit('set_pos', (self.currentposition, self.constantSpeedSpinBox.value(), 50))
                pass
#                self.motorCommandNeedsToBeIssued.emit('stop', ())
            elif controlmethod == MotorControlMethod.ConstantSpeedOnPress:
                pass
            elif controlmethod == MotorControlMethod.ConstantAccelerationOnPress:
                speed = int(self.currentsetspeed + self.accelerationDoubleSpinBox.value() * deltat)
                if abs(speed) <= self.maxSpeedSpinBox.value():
                    self.motorCommandNeedsToBeIssued.emit('set_speed', (speed,))
        if self.minusbuttonpressedat is not None:
            deltat = time.monotonic() - self.minusbuttonpressedat
            if controlmethod == MotorControlMethod.IncrementSpeedOnPress:
                pass
            elif controlmethod == MotorControlMethod.Interval:
#                self.motorCommandNeedsToBeIssued.emit('set_pos', (self.currentposition, self.constantSpeedSpinBox.value(), 50))
 #               self.motorCommandNeedsToBeIssued.emit('stop', ())
                pass
            elif controlmethod == MotorControlMethod.ConstantSpeedOnPress:
                pass
            elif controlmethod == MotorControlMethod.ConstantAccelerationOnPress:
                speed = int(self.currentsetspeed - self.accelerationDoubleSpinBox.value() * deltat)
                if abs(speed) <= self.maxSpeedSpinBox.value():
                    self.motorCommandNeedsToBeIssued.emit('set_speed', (speed,))

