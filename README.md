GUI for a LEGO(R) passenger train
=================================

Installation:
-------------

$ python setup.py develop

Requirements:
-------------

- PySide2
- curio
- bricknil
- click
